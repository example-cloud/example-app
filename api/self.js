import { useGet, usePost, usePut } from "@/utils/request"

const contentPath = '/system/self'

export const register = (param) => {
  return usePost(`${contentPath}/register`, param)
}

export const reset = (param) => {
  return usePut(`${contentPath}/reset`, param)
}

export const resetPassword = (param) => {
  return usePut(`${contentPath}/reset_password`, param)
}

export const selfInfo = () => {
  return useGet(`${contentPath}/info`)
}
