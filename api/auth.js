import { usePost } from '@/utils/request.js'

const contentPath = '/auth/oauth'

export const login = (param) => {
  return usePost(`${contentPath}/token?grant_type=mobile_password`, {mobile: param.phone, password: param.password}, {
    header: {
      Authorization: 'Basic ZXhhbXBsZS1hcHA6ZXhhbXBsZS1hcHA=',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

export const logout = () => {
  return usePost(`${contentPath}/logout`)
}
