import { usePost } from "@/utils/request";

const contentPath = '/system/sms'

export const sendCode = (param) => {
  return usePost(`${contentPath}/code/${param?.phone}`, {'bizCode': param.bizCode}, {
    header: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}
