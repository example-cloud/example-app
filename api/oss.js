import { useUpload } from "@/utils/request";

export const uploadFile = (filePath) => {
	return useUpload('/storage/upload', filePath)
}