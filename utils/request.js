export const baseUrl = 'http://192.168.3.65:2022'

// 全局请求封装
const useRequest = (method, url, data = {}, header = {}) => {
	const token = uni.getStorageSync('lifeData')?.vuex_token?.access_token
	if(token && !header.Authorization){
		header.Authorization = `Bearer ${token}`
	}
	header.tenantId = 'e_cloud'
return new Promise((resolve, reject) => {
	uni.request({
		url: baseUrl + url,
		method,
		data,
		header,
		success: (res) => {
			if(res.statusCode === 200) {
				resolve(res.data)
			} else {
				reject(res.data)
			}
		},
		fail(err) {
			reject(err);
		}
	})
})}

// POST 请求
export const usePost = (url, data = {}, {
	header = {}
} = { params: {}, header: {} }) => {
	return useRequest('POST', url, data, header)
}

// PUT 请求
export const usePut = (url, data = {}, {
	header = {}
} = { params: {}, header: {} }) => {
	return useRequest('PUT', url, data, header)
}

// DELETE 请求
export const useDelete = (url, {
	params = {},
	header = {}
} = { params: {}, header: {} }) => {
	return useRequest('DELETE', url, params, header)
}


// GET 请求
export const useGet = (url, {
	params = {},
	header = {}
} = { params: {}, header: {} }) => {
    return useRequest('GET', url, params, header)
}

export const useUpload = (url, filePath) => {
    const token = uni.getStorageSync('lifeData')?.vuex_token?.access_token
    return new Promise((resolve, reject) => {
        uni.uploadFile({
            url: baseUrl + url,
            header: {
                Authorization: `Bearer ${token}`
            },
            name: 'file',
            filePath,
            success(ret) {
                if (ret.statusCode === 200) {
                    resolve(JSON.parse(ret.data))
                } else {
                    reject(ret)
                }
            },
            fail(err) {
                reject(err)
            }
        })
    })
}
